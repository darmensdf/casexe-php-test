<h1>Поздравляем!</h1>
<p>
    Ваш выигрыш – <strong><?php echo $params['prize'];?></strong>
</p>

<form action="index.php?action=receive" method="post" id="form-receive">
    <input type="hidden" name="prize" value="<?php echo base64_encode(serialize($params['prize']));?>">
    <button type="submit" id="form-receive-submit">Получить!</button>
</form>

<div id="form-receive-result"></div>

<form action="index.php?action=manage" method="post">
    <?php if($params['prize']->manageTitle):?>
        <button type="submit"><?php echo $params['prize']->manageTitle;?></button>
    <?php endif;?>
</form>
