<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/14/18
 * Time: 12:36 AM
 */

namespace Casexe;

class App
{
    protected $action;
    protected $getParams;
    protected $postParams;
    protected $session;
    /**
     * @var array
     */
    public $warehouse;

    protected $prizeClasses = ['Money', 'Points', 'PhysicalThing'];

    public $coefficient;

    public function __construct($action, $coefficient = 2)
    {
        $this->action = $action;
        $this->coefficient = $coefficient;

        $this->warehouse = json_decode(file_get_contents('db/warehouse.json'), true);
        if (!count($this->warehouse['things']))
        {
            unset($this->prizeClasses[2]);
        }

        $this->getParams = $_GET;
        $this->postParams = $_POST;

        session_name('casexe');
        session_start();
        $this->session = &$_SESSION;
    }

    public function run()
    {
        if (method_exists($this, $this->action))
        {
            echo $this->{$this->action}();
            $this->flash(null);
        } else {
            echo $this->notFound();
        }
    }

    protected function notFound(): string
    {
        return $this->_render('404', ['title' => 'Страница не найдена']);
    }

    protected function redirect($action)
    {
        header('Location: index.php?action='.$action);
        exit;
    }

    protected function flash($message)
    {
        if ($message === null)
        {
            if (isset($this->session['message'])) unset($this->session['message']);
        } else {
            $this->session['message'] = $message;
        }
    }

    protected function auth()
    {
        if ($this->_isAuthenticated())
        {
            $this->redirect('prize');
        }

        if ($this->_isMethod('GET'))
        {
            return $this->_render('auth');
        } else {
            $username = $this->postParams['username'] ?? null;
            $password = $this->postParams['password'] ?? null;

            $users = json_decode(file_get_contents('db/users.json'), true);
            foreach ($users as $user) {
                if (($user['username'] == $username) && ($user['password'] == $password))
                {
                    $this->session['username'] = $username;
                    $this->redirect('prize');
                }
            }

            $this->flash('Неверный логин и/или пароль');
            $this->redirect('index');
        }
    }

    public function index()
    {
        $this->redirect('prize');
    }

    public function logout()
    {
        if (!$this->_isAuthenticated())
        {
            $this->redirect('index');
        }

        unset($this->session['username']);

        if (isset($this->session['prize']))
        {
            unset($this->session['prize']);
        }

        session_regenerate_id(true);
        $this->redirect('index');
    }

    public function prize()
    {
        if (!$this->_isAuthenticated())
        {
            $this->redirect('auth');
        }

        if ($this->_isMethod('post'))
        {
            $index = rand(0, count($this->prizeClasses) - 1);

            $className = '\Casexe\\'.$this->prizeClasses[$index];
            $prize = new $className($this);
            $prize->generate();

            $this->session['prize'] = $prize;

            return $this->_render('prize/result', ['prize' => $prize]);
        } else {
            return $this->_render('prize/index');
        }
    }

    public function receive()
    {
        if (!$this->_isAuthenticated())
        {
            $this->redirect('auth');
        }

        if (!$this->_isAjax())
        {
            $this->redirect('404');
        }


        $prize = unserialize(base64_decode($this->getParams['prize']));
        $this->_addPrize($prize);
        $prize->receive();

        return $this->_render('prize/receive', ['prize' => $prize], false);
    }

    public function myprizes()
    {
        if (!$this->_isAuthenticated())
        {
            $this->redirect('auth');
        }

        $prizes = $this->_getUserData('prizes', []);

        return $this->_render('prize/my', ['items' => $prizes]);
    }


    public function manage()
    {
        if (
            $this->_isMethod('post') &&
            isset($this->session['prize']) &&
            ($this->session['prize'] instanceof Prize)
        )
        {
            /**
             * @var Prize $prize
             */
            $prize = $this->session['prize'];
            $result = $prize->manage();
            unset($this->session['prize']);
            if (is_array($result))
            {
                $this->flash($result[1]);
            }
            return $this->_render('prize/index');
        } else {
            $this->redirect('404');
        }
    }

    protected function _render($view, $params = [], $layout = true): string
    {
        ob_start();

        if ($layout)
        {
            require('inc/header.php');
        }

        require('views/'.$view.'.php');

        if ($layout)
        {
            require('inc/footer.php');
        }

        return ob_get_clean();
    }

    protected function _isAjax(): bool
    {
        // source: https://davidwalsh.name/detect-ajax
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    protected function _isAuthenticated(): bool
    {
        return isset($this->session['username']);
    }

    protected function _isMethod(string $method): bool
    {
        return $_SERVER['REQUEST_METHOD'] == strtoupper($method);
    }

    protected function _getUserData($key, $default = null)
    {
        $users = json_decode(file_get_contents('db/users.json'), true);
        foreach ($users as $user) {
            if ($user['username'] == $this->session['username'])
            {
                return $user[$key] ?? $default;
            }
        }
    }

    protected function _setUserData($key, $value)
    {
        $file = 'db/users.json';
        $users = json_decode(file_get_contents($file), true);
        foreach ($users as &$user) {
            if ($user['username'] == $this->session['username'])
            {
                $user[$key] = $value;
            }
        }

        file_put_contents($file, json_encode($users));
    }

    public function _getBonuses()
    {
        return $this->_getUserData('bonuses', 0);
    }

    public function _addBonuses($delta)
    {
        $bonuses = $this->_getBonuses();
        $bonuses += $delta;
        $this->_setBonuses($bonuses);
    }

    public function _setBonuses($bonuses)
    {
        $this->_setUserData('bonuses', $bonuses);
    }

    public function _addPrize(Prize $prize)
    {
        $prizes = $this->_getUserData('prizes');
        $prizes[] = $prize;
        $this->_setUserData('prizes', $prizes);
    }

    public function _syncWarehouse()
    {
        file_put_contents('db/warehouse.json', json_encode($this->warehouse));
    }

}