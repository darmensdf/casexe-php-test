<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/14/18
 * Time: 12:34 AM
 */

namespace Casexe;

class Money extends Prize
{
    public $manageTitle = 'Конвертировать в баллы';

    public function generate()
    {
        $this->content = rand(100, $this->app->warehouse['money']);
    }

    public function getName()
    {
        return 'Деньги';
    }

    public function __toString()
    {
        return sprintf('%s: $%d', $this->getName(), $this->getContent());
    }

    public function manage()
    {
        $pointsPrize = new Points($this->app, $this->content * $this->app->coefficient);
        $pointsPrize->receive();
        $this->app->_addPrize($pointsPrize);

        return [$pointsPrize, sprintf("%s сконвертированы в %s", $this, $pointsPrize)];
    }

    public function receive()
    {
        $this->app->warehouse['money'] -= $this->getContent();
        $this->app->_syncWarehouse();
    }
}