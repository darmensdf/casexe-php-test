<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/14/18
 * Time: 12:36 AM
 */

namespace Casexe;

class PhysicalThing extends Prize
{
    public $manageTitle = false;

    public function generate()
    {
        $items = $this->app->warehouse['things'];

        $min = 0;
        $max = count($items) - 1;
        $index = rand($min, $max);

        $this->content = $items[$index];
    }

    public function getName()
    {
        return 'Физический предмет';
    }

    public function __toString()
    {
        return sprintf('%s "%s"', $this->getName(), $this->getContent());
    }

    public function receive()
    {
        $pos = array_search($this->getContent(), $this->app->warehouse['things']);
        if ($pos !== false)
        {
            unset($this->app->warehouse['things'][$pos]);
            $this->app->warehouse['things'] = array_values($this->app->warehouse['things']);
            $this->app->_syncWarehouse();
        }
    }

    public function manage()
    {
        return true;
    }
}