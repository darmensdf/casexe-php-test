<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/14/18
 * Time: 12:32 AM
 */

namespace Casexe;

abstract class Prize
{
    protected $content;

    /**
     * @var App
     */
    protected $app;

    public $cancelTitle;

    abstract public function __toString();
    abstract public function generate();
    abstract public function getName();
    abstract public function manage();
    abstract public function receive();

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function getContent(){
        return $this->content;
    }


}