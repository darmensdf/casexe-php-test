<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/14/18
 * Time: 12:35 AM
 */

namespace Casexe;

class Points extends Prize
{
    public $manageTitle = false;

    public function __construct($app, $content = null)
    {
        parent::__construct($app);
        $this->content = $content;
    }

    public function generate()
    {
        $this->content = rand(2000, 10000);
    }

    public function getName()
    {
        return 'Бонусные баллы';
    }

    public function __toString()
    {
        return sprintf('%s: %d б.', $this->getName(), $this->getContent());
    }

    public function receive()
    {
        $this->app->_addBonuses($this->getContent());
    }

    public function manage()
    {

    }
}