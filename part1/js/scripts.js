$(function(){
    $('#form-receive').on('submit', function(e){
        e.preventDefault();
        $('#form-receive-submit').attr('disabled', 'disabled');
        $.get(this.action, $(this).serializeArray(), function(html){
            $('#form-receive-result').show();
            $('#form-receive-result').html(html);
        });
    });
});