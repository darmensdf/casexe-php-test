<?php
/**
 * Crafted with <3 by Darmen Amanbayev
 * Date: 9/13/18
 * Time: 9:37 PM
 */

include 'src/Prize.php';
include 'src/Money.php';
include 'src/Points.php';
include 'src/PhysicalThing.php';
include 'src/App.php';


$action = $_GET['action'] ?? 'index';
$app = new \Casexe\App($action);
$app->run();