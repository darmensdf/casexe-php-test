<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $params['title'] ?? 'Без заголовка';?></title>
        <link rel="stylesheet" href="/css/styles.css">
    </head>

    <body>
        <div class="container">
            <?php if (isset($_SESSION['message'])):?>
                <div class="flash">
                    <?php echo $_SESSION['message'];?>
                </div>
            <?php endif;?>

            <div class="menu">
                <a href="/index.php?action=prize">Получить приз</a> | <a href="/index.php?action=myprizes">Мои призы</a>
            </div>
