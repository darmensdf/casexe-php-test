@extends('layout')

@section('title') {{ $prize }} @endsection

@section('content')
    <h1>Поздравляем!</h1>
    <p>
        Ваш выигрыш – <strong>{{ $prize }}</strong>
    </p>

    @if($prize->user_id !== auth()->user()->id)
        <form action="{{ route('prize_receive', ['id' => $prize->id]) }}" method="post" id="form-receive">
            <button type="submit" id="form-receive-submit">Получить!</button>
        </form>
    @endif

    <div id="form-receive-result"></div>

    @if ($prize->type == 'money')
        <form action="{{ route('prize_convert', ['id' => $prize->id]) }}" method="post">
            @csrf
            <button type="submit">Конвертировать в {{ number_format(config('lottery.coefficient') * (int) $prize->content) }} баллов лояльности</button>
        </form>
    @endif

    <br>

    <form action="{{ route('prize_refuse', ['id' => $prize->id]) }}" method="post">
        @csrf
        <button type="submit">Отказаться</button>
    </form>

@endsection
