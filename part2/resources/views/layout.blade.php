<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    </head>

    <body>
        <div class="container">
            @if(session('message'))
                <div class="flash">
                    {{ session('message') }}
                </div>
            @endif
            <div class="menu">
                <a href="{{ route('home') }}">Получить приз</a> | <a href="{{ route('myprizes') }}">Мои призы</a>
            </div>

            @yield('content')

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>
    </body>
</html>
