@extends('layout')

@section('title') Мои призы @endsection

@section('content')
    <ul>
        @foreach($prizes as $prize)
            <li>
                <a href="{{ route('prize_detail', ['id' => $prize->id]) }}">{{ $prize }}</a>
            </li>
        @endforeach
    </ul>
@endsection
