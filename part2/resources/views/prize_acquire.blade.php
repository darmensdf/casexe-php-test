@switch($prize->type)
    @case('money')
        Деньги будут переведны на Ваш банковский счет.
        @break
    @case('points')
        Баллы зачислены на Ваш счет.
        @break
    @case('physical')
        {{ $prize }} отправлен вам на почту.
@endswitch
