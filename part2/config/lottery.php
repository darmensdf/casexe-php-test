<?php

return [
    'limits' => [
        'money' => 50000,
        'physical' => ['Самолет', 'Яхта', 'Подводная лодка'],
    ],

    'types' => [
        'money',
        'points',
        'physical'
    ],

    'range' => [
        'points' => [1, 4000]
    ],

    'coefficient' => 2
];
