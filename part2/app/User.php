<?php

namespace App;

use App\Models\Prize;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function prizes()
    {
        return $this->hasMany('App\Models\Prize');
    }

    public function acquire(Prize $prize)
    {
        $this->prizes()->save($prize);

        if ($prize->type == 'money')
        {
            $this->money += (int) $prize->content;

            $apiResult = true; // TODO: API calls to bank system
            if ($apiResult)
            {
                $this->save();
            }
        }

        if ($prize->type == 'points')
        {
            $this->points += (int) $prize->content;
            $this->save();
        }
    }

    public function refuse(Prize $prize)
    {
        $prize->delete();

        if ($prize->type == 'money')
        {
            $this->money -= (int) $prize->content;
            $this->save();
        }

        if ($prize->type == 'points')
        {
            $this->points -= (int) $prize->content;
            $this->save();
        }
    }
}
