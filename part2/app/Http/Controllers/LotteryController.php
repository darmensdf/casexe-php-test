<?php

namespace App\Http\Controllers;

use App\Jobs\AcquirePrize;
use App\Jobs\ConvertMoney;
use App\Jobs\RefusePrize;
use App\Models\Prize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LotteryController extends Controller
{
    public function index(Request $request)
    {
        return view('index');
    }

    public function myPrizes(Request $request)
    {
        $prizes = Auth::user()->prizes;
        return view('my_prizes', compact('prizes'));
    }

    public function prize(Request $request)
    {
        $types = config('lottery.types');
        $className = '\App\Models\\'.ucfirst(array_random($types));

        $prize = new $className;

        if ($prize->save())
        {
            return redirect()->route('prize_detail', ['id' => $prize->id]);
        }
    }

    public function prizeDetail($id)
    {
        $prize = Prize::findOrFail($id);
        return view('prize_detail', compact('prize'));
    }

    public function prizeReceive(Request $request, $id)
    {
        if (!$request->isXmlHttpRequest())
        {
            abort(403);
        }

        $prize = Prize::findOrFail($id);
        AcquirePrize::dispatch(Auth::user(), $prize)->onQueue($prize->type);;

        return view('prize_acquire', compact('prize'));
    }

    public function prizeRefuse($id)
    {
        $prize = Prize::findOrFail($id);
        RefusePrize::dispatch(Auth::user(), $prize)->onQueue($prize->type);

        return redirect()
            ->route('home')
            ->with('message', 'Вы отказались от приза.');
    }

    public function prizeConvert($id)
    {
        $prize = Prize::findOrFail($id);
        if ($prize->type !== 'money')
        {
            abort(403);
        }

        ConvertMoney::dispatch(Auth::user(), $prize)->onQueue($prize->type);;
        return redirect()
            ->route('home')
            ->with('message', 'Денежные средства сконвертированы в баллы.');
    }
}
