<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    public function holder()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function __toString()
    {
        switch ($this->type)
        {
            case 'money':
                return sprintf('Деньги: $%d', $this->content);
            case 'points':
                return sprintf('Баллы: %d б', $this->content);
            case 'physical':
                return sprintf('Предмет: «%s»', $this->content);
            default:
        }
    }

    public function convert()
    {
        if ($this->type !== 'money')
        {
            return false;
        }

        $coefficient = config('lottery.coefficient');
        $newPrize = new Points(['content' => (int) $this->content * $coefficient]);
        return $newPrize;
    }
}
