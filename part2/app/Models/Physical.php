<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Physical extends Prize
{
    protected $table = 'prizes';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setAttribute('type', 'physical');

        $content = array_random(config('lottery.limits')['physical']);
        $this->setAttribute('content', $content);
    }
}
