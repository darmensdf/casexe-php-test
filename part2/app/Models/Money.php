<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Money extends Prize
{
    protected $table = 'prizes';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setAttribute('type', 'money');

        $content = rand(1, config('lottery.limits')['money']);
        $this->setAttribute('content', $content);
    }
}
