<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Points extends Prize
{
    protected $table = 'prizes';
    protected $fillable = ['content'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setAttribute('type', 'points');

        if (!isset($attributes['content']))
        {
            $content = rand(config('lottery.range')['points'][0], config('lottery.range')['points'][1]);
            $this->setAttribute('content', $content);
        }
    }
}
