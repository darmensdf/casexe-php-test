<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class sendMoney extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'casexe:send-money';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends money prizes to bank accounts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('queue:work', [
            '--queue' => 'money',
            '--stop-when-empty' => true,
        ]);
    }
}
