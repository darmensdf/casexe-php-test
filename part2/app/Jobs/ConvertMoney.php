<?php

namespace App\Jobs;

use App\Models\Prize;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ConvertMoney implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Money
     */
    protected $prize;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Prize $prize
     */
    public function __construct(User $user, Prize $prize)
    {
        $this->user = $user;
        $this->prize = $prize;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $newPrize = $this->prize->convert();
        $newPrize->save();

        $this->prize->delete();
        AcquirePrize::dispatch($this->user, $newPrize)->onQueue($newPrize->type);;
    }
}
