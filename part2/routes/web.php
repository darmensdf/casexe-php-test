<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::middleware(['auth'])->group(function(){

    Route::get('/', 'LotteryController@index')
        ->name('home');

    Route::post('prize', 'LotteryController@prize')
        ->name('prize');

    Route::get('prize/{id}', 'LotteryController@prizeDetail')
        ->name('prize_detail');

    Route::post('prize/{id}/receive', 'LotteryController@prizeReceive')
        ->name('prize_receive');

    Route::post('prize/{id}/refuse', 'LotteryController@prizeRefuse')
        ->name('prize_refuse');

    Route::post('prize/{id}/convert', 'LotteryController@prizeConvert')
        ->name('prize_convert');

    Route::get('my-prizes', 'LotteryController@myPrizes')
        ->name('myprizes');
});
