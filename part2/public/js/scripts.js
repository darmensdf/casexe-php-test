$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function(){
    $('#form-receive').on('submit', function(e){
        e.preventDefault();
        $('#form-receive-submit').attr('disabled', 'disabled');
        $.post(this.action, $(this).serializeArray(), function(html){
            $('#form-receive-result').show();
            $('#form-receive-result').html(html);
        });
    });
});
