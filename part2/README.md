# Casexe PHP Test on Laravel

## Запуск
1. Выставить параметр `QUEUE_CONNECTION` в значение `database` в файле `.env`
2. Выполнить команды:
    - `php artisan migrate`
    - `php artisan db:seed`
    - `php artisan queue:work`
3. Запустить приложение. Логин `user@gmail.com`, пароль `1234`
4. Запустить обработчики очередей (см. ниже)    

## Очереди
Логика отгрузки призов реализована в виде очередей. Очередь накапливается в базе в таблице `jobs`. Для каждого типа приза (points, money, physical) – своя очередь:
- Получение, конвертирование денег: `php artisan queue:work --queue=money`
- Получение бонусов: `php artisan queue:work --queue=points`
- Получение физического приза: `php artisan queue:work --queue=physical`

## Отправить деньги на банковский счет пользователя
`php artisan casexe:send-money`

## Запустить Unit-тест
`vendor/bin/phpunit`
